# Exercise 35: Docker image history

In the case of analysing images built by different people
you may need to verify what's inside

## Check history for onestage image 

```bash 
docker image history one-stage

IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
580d9a3773c3        3 days ago          /bin/sh -c #(nop)  CMD ["./app"]                0B
1d273b69b751        3 days ago          /bin/sh -c go build -o app .                    2.06MB
9470164ceeb4        3 days ago          /bin/sh -c #(nop) COPY file:df60b2671a451d45…   171B
8e4e0d53dabd        3 days ago          /bin/sh -c #(nop) WORKDIR /code/                0B
fc0a21dde945        3 days ago          /bin/sh -c #(nop)  LABEL maintainer=tomasz.s…   0B
4a581cd6feb1        3 days ago          /bin/sh -c #(nop) WORKDIR /go                   0B
<missing>           3 days ago          /bin/sh -c mkdir -p "$GOPATH/src" "$GOPATH/b…   0B
<missing>           3 days ago          /bin/sh -c #(nop)  ENV PATH=/go/bin:/usr/loc…   0B
<missing>           3 days ago          /bin/sh -c #(nop)  ENV GOPATH=/go               0B
<missing>           3 days ago          /bin/sh -c set -eux;   dpkgArch="$(dpkg --pr…   363MB
<missing>           5 days ago          /bin/sh -c set -ex;  if ! command -v gpg > /…   17.5MB
```

## Use [dive](https://github.com/wagoodman/dive) tool

* Check images from multistage build
```bash 
dive one-stage:latest
dive multi-stage:latest
dive multi-stage-scratch:latest
```

[go to home](../../../README.md)
