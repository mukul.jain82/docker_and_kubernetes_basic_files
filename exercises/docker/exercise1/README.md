# Exercise 1: Hello World 


## Run hello-world image:
```bash 
docker run hello-world
```

## Run debian image with command echo:
```bash 
docker run debian echo "Hello World"
```
[go to home](../../../README.md)

[go to next](../exercise2/README.md)
