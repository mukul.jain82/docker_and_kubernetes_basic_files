# Exercise 11: Docker foreground vs detached

Compare foreground and detached mode at a container, which prints only hostname and end process.

## Create container in foreground mode
```bash 
docker run --name=test-foreground debian:latest hostname
```
## Create container in detached mode
```bash 
docker run -d  --name=test-detached debian:latest hostname
```

## Print logs for container using Hash ID 
```bash 
docker logs (contaner ID)
```

[go to home](../../../README.md)

[go to next](../exercise12/README.md)
