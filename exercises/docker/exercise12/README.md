# Exercise 12: Docker labels

When the container is created all labels from image are added to the container.
It allows to add the custom labels with run command.

## Create container and add label deployer
```bash 
docker run -d --name labels -l deployer=Tomek ubuntu:latest sleep 600
```

## List all containers showing labels using format option: 
```bash 
docker ps -a --format "table {{.ID}}\t{{.Names}}\t{{.Labels}}"
```
## List all containers based on labels:

```bash 
docker ps -a -f label=deployer=Tomek
```

[go to home](../../../README.md)

[go to next](../exercise13/README.md)


