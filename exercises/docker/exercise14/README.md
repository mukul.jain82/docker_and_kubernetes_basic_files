# Exercies 14: Docker dns

Docker server also copy file /etc/resolv.conf, the same as file hostname.
To overwrite default values, option --dns should be used.

## Create container with custom DNS Nameservers
```bash
docker run --rm=true -it --name=test-dns --dns=8.8.8.8 --dns=8.8.4.4 --dns-search=example.com ubuntu:latest  /bin/bash
```

Inside container:
```bash
cat /etc/resolv.conf
```

## Exit container:
```bash
CTRL-D
```

[go to home](../../../README.md)

[go to next](../exercise15/README.md)

