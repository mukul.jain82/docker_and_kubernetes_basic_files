# Exercise 31: run wordpress application in docker-compose

1. Change directory
```bash
cd ~/workshop/exercises/docker/exercise31/
```

2. Check docker-compose.yaml
```bash
cat docker-compose.yaml
```

3. Create stack based on docker-compose file:

```bash
docker-compose up -d
```

4. Check container status:

```bash
docker-compose ps
```

5. Check logs:

```bash
docker-compose logs
```

6. Check www page:
[http://127.0.0.1](http://127.0.0.1)

7. Clean: delete containers and volumes :
```bash
docker-compose down --rmi local --volumes
```

[go to home](../../../README.md)

[go to next](../exercise32/README.md)
