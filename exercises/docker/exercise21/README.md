# Exercise 21: Docker log

## Create container based on nginx image
```bash 
docker run --rm -d -p 80:80 --name test-nginx nginx:1.15
```
## Check running containers and logs 
```bash 
docker ps
curl 127.0.0.1
docker logs test-nginx
```
## Inspect container and find information about logs
```bash 
docker inspect test-nginx
```
## Print logs directly from host 
```bash 
$ sudo cat /var/lib/docker/containers/<hash>/<hash>-json.log
```
## Print logs using docker clinet
```bash 
docker logs -f test-nginx
```

## In the second terminal request application several times
```bash 
curl 127.0.0.1:80
```

[go to home](../../../README.md)

[go to next](../exercise22/README.md)
