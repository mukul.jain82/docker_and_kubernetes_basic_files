# Exercise 4: Docker search

## Search official debian image 

```bash 
docker search -f is-official=true -f stars=3  debian
```

## Search debian image with minium 3 starts
```
docker search -f stars=3  debian
```

[go to home](../../../README.md)

[go to next](../exercise5/README.md)
