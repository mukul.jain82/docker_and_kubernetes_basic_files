# Exercise 7: Docker creating containter

## Using run command 
### In first terminal start showing all events 
```bash 
docker events
```
### In second terminal start container by command run 
```bash 
docker run --name test-create debian:latest sleep 10
```

## Using separately create and start command

### Keep still open docker events in first terminal 
### Create container
```bash 
docker create --name test-create2 debian:latest sleep 10
```
### Start container 
```bash 
docker start test-create2
```
### List all running containers 
```bash 
docker ps
```

[go to home](../../../README.md)

[go to next](../exercise8/README.md)
