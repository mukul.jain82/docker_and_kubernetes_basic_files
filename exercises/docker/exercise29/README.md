# Exercise 29: Dockerfile multi-stage build

1. Pulling repository with example app:
```bash
git clone https://github.com/tszymanski/multi-stage-build.git
```
2. Build images: 
```bash 
cd multi-stage-build.git
docker build -t one-stage:latest -f Dockerfile .
docker build -t multi-stage:latest -f Dockerfile_multistage .
docker build -t multi-stage-scratch:latest -f Dockerfile_multistage_scratch .
```

3. Check images size 
```bash 
docker images | grep stage
```

[go to home](../../../README.md)

[go to next](../exercise30/README.md)
