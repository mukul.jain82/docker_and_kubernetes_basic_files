# Exercise 2: Connect directly to  API

## Create container by connecting direct to API using curl 
```bash 
HASH=$(curl --unix-socket /var/run/docker.sock -H "Content-Type: application/json" -d '{"Image": "debian", "Cmd": ["echo", "hello world"]}' -X POST http:/v1.24/containers/create | jq -r '.Id')
```

## Start container 
```bash 
curl --unix-socket /var/run/docker.sock -X POST http:/v1.24/containers/${HASH}/start
```

## Pull logs from started container
```bash 
curl --unix-socket /var/run/docker.sock "http:/v1.24/containers/${HASH}/logs?stdout=1"  | xargs -0 echo
```

[go to home](../../../README.md)

[go to next](../exercise3/README.md)
