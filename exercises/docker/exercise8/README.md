# Exercise 8: Docker name

## List all created containers
```bash 
docker ps -a 
```
## Create container with custom name 
```bash 
docker run --name=awesome-service -d debian:latest sleep 10
```

## List all running containers
```bash 
docker ps 
```

[go to home](../../../README.md)

[go to next](../exercise9/README.md)
