# Exercise 9: Docker run --rm

## Run container without flag --rm
### In first terminal create container
```bash
docker run --name test-rm debian:latest sleep 10
```

### In second terminal:
#### List all running containers
```bash
docker ps
```
#### Remove container test-rm
```bash
docker rm test-rm
```

## Run container with flag --rm

### In first terminal watch list of all running containers
```bash
watch -n 1 -d "docker ps"
```


### In second terminal run container with flag --rm

```bash
docker run --rm --name test-rm debian:latest sleep 10
```

### Check all containers list
```bash
docker ps -a
```

[go to home](../../../README.md)

[go to next](../exercise10/README.md)
