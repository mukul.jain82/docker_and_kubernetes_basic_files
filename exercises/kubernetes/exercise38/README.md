# Exercise 38: Service my-service -> www.interia.pl

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise38/
```

2. Create an ExternalName service
```bash
kubectl apply -f service-external-name.yaml
```bash

3. Check service
```bash
kubectl exec dnsutils -- dig my-service A +search

my-service.test-network.svc.cluster.local. 30 IN CNAME www.interia.pl
www.interia.pl.     30  IN  A   217.74.76.129
www.interia.pl.     30  IN  A   185.69.194.129
```

4. Clean
```bash
kubens -
kubectl delete namespace test-network
```


[go to home](../../../README.md)

[go to next](../exercise39/README.md)
