# Exercise 25: Update and rollback

1. Create a namespace test-rolling
```bash
kubectl create namespace test-rolling
kubens test-rolling
```

2. Create an application based on nginx image
```bash
kubectl create deployment rollingnginx --image=nginx:1.8
```
3. Update an image: change tag to 1.15
```bash
kubectl edit deployments.apps rollingnginx
```

4. Check the deployment history
```bash
kubectl rollout history deployment
kubectl rollout history deployment rollingnginx --revision=1
kubectl rollout history deployment rollingnginx --revision=2
```

4. Print all objects connected to new application
```bash 
kubectl get all <--selector|-l> app=rollingnginx
```

5. Watch events
```bash
kubectl get events --watch
```

6. In the second terminal rollback deployment
```bash
kubectl rollout undo deployment rollingnginx --to-revision=1
```
7. Clean
```bash
kubens -
kubectl delete namespace test-rolling
```

[go to home](../../../README.md)

[go to next](../exercise26/README.md)
