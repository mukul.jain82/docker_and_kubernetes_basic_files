# Exercise 44: Create rule for user test-user

1. Change a context to the default
```bash
kubectl config use-context minikube
```

2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise44/
```

3. Apply the role and role-binding
```bash
kubectl apply -f role.yaml
kubectl apply -f role-binding.yaml
```

4. Check roles and role-bindings  
```bash
kubectl get roles
kubectl get rolebindings
```

5. Change a context
```bash
kubectl config use-context test-user-context
kubectl get pods
kubectl get all
kubectl create namespace test
```

6. Change the context to the default
```bash
kubectl config use-context minikube
```

[go to home](../../../README.md)

[go to next](../exercise45/README.md)
