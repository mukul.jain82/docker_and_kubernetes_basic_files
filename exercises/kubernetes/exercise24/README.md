# Exercise 24: The Blue/Green strategy

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise24
```

2. Create the first application
```bash
kubectl apply -f app-v1.yaml
```

3. Watch pod changes
```bash
watch kubectl get po
```

4. In the second terminal check an application
```bash
while true; do sleep 0.1; curl $(minikube service my-app --url -n test-deploy); done
```

5. In the third terminal Create a second application
```bash
kubectl apply -f app-v2.yaml
```

6. Change a pod selector for service my-app by changing version value to '2.0.0'
```bash
kubectl patch service my-app -p '{"spec":{"selector":{"version":"v2.0.0"}}}'
```

7. Change selector to the previous value
```bash
kubectl patch service my-app -p '{"spec":{"selector":{"version":"v1.0.0"}}}'
```

8. Clean
```bash
kubens -
kubectl delete namespace test-deploy
```

[go to home](../../../README.md)

[go to next](../exercise25/README.md)
