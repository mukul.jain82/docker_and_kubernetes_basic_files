# Exercise 18: Create DeamonSet

1. Create new namespace
```bash
kubectl create namespace test-deamonset
kubens test-deamonset
```

2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise18/
```

3. Create a POD
```bash
kubectl apply -f deamonset-fluentd.yaml
```

4. Check a POD 
```bashs
kubectl exec -it fluentd-elasticsearch-... -- bash
```

5. Clean
```bash
kubens default
kubectl delete namespace test-deamonset
```

[go to home](../../../README.md)

[go to next](../exercise19/README.md)
