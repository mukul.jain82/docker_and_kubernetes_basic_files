# Exercise 19: Test statefulset application

1. Create namespce and application
```bash
kubectl create namespace test-statefulset
kubens test-statefulset
```

2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise19/
```

3. Create StateFulset
```bash
kubectl apply -f my-application-statefulset.yaml
```

4. Check a status
```bash
kubectl get sts
kubectl get all
```

5. Check PersistentVolumeClaim
```bash
kubectl get pvc
```

6. Clean
```bash
kubens -
kubectl delete namespace test-statefulset
```

[go to home](../../../README.md)

[go to next](../exercise20/README.md)
