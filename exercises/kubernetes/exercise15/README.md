# Exercise 15: Test default limits in namespace

1. Create new namespace and switch to it:
```bash
kubectl create namespace limit-example
kubens limit-example
```
2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise15/
```

3. Add limits:
```bash
kubectl apply -f limits.yaml
```

4. Check limits
```bash
kubectl describe limits
```

5. Create POD:
```bash
kubectl apply -f busybox.yaml
```

6. Check Pod
```bash
kubectl describe pod busybox
```

7. Clean namespace:
```bash
kubens -
kubectl delete namespace limit-example
```

[go to home](../../../README.md)

[go to next](../exercise16/README.md)
