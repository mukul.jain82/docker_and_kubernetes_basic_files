# Exercise 2: Create namespace

## Using kubectl command
1. show all namespaces
```bash
kubectl get namespaces
```

2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise2/
```

3. Create new namespaces
```bash
kubectl apply -f namespace.yaml
```
4. Show kubectl configuration
* All config
```bash
kubectl config view
```
* Current context
```bash
kubectl config current-context
```
5. Set new context for new namespaces
```bash
kubectl config set-context dev --namespace=development --cluster=minikube --user=minikube
kubectl config set-context prod --namespace=production --cluster=minikube --user=minikube
```
6. Show all configuration files
```bash
kubectl config view
```
7. Change to dev context
```bash
kubectl config use-context dev
kubectl config current-context
```
8. Create pod in new namespace
```bash
kubectl create -f busybox.yaml
kubectl get all
```

9. Change context to prod and create pod
```bash
kubectl config use-context prod
kubectl create -f busybox.yaml
```
10. Check all pods
```bash
kubectl get all
kubectl get all -A
```

11. Change back to default context
```bash
kubectl config use-context minikube
```

## Using Kubens command

kubens - helps you switch between Kubernetes namespaces smoothly

1. Show all available options:
```bash
kubens -h
```

2. Switch to development namespaces
* Switch namespace
```bash
kubens development
```
* check current namespace
```bash
kubens -c
```
* Print all objects from namespace
```bash
kubectl get all
```

3. Switch back to default namespace
```bash
$ kubens -
$ kubens -c
$ kubectl get all
```

[go to home](../../../README.md)

[go to next](../exercise3/README.md)
