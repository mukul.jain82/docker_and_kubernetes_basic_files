# Exercise 41: Using the PVC

1. Create a PVC
```bash
kubectl apply -f pv-clame.yaml
```

2. Check
* PVC
```bash
kubectl get pvc
kubectl describe pvc task-pv-claim
```
* PV
```bash
$ kubectl get pv
$ kubectl describe pv
```

3. Create a Pod with PVC
```bash
kubectl apply -f pv-pod.yaml
kubectl describe pod task-pv-pod
```

4. Clean
```bash
kubectl delete -f pv-pod.yaml
kubectl delete -f pv-clame.yaml
```



[go to home](../../../README.md)

[go to next](../exercise42/README.md)
