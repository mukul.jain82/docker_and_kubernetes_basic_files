# Exercise 45: Start pod with set UID

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise45/
```

2. Create a namespace
```bash
kubectl create namespace test-security
kubens test-security
```

3. Create a pod with security context
```bash
kubectl create -f busybox-uid.yaml
```
4. Check
```bash
kubectl exec -it busybox-uid -- sh
```
5. Create a pod with readonly filesystem
```bash
kubectl create -f busybox-readonly.yaml
```

6. Check
```bash
kubectl exec -it busybox-readonly -- touch x
```

7. Leave namespace for next exercise



[go to home](../../../README.md)

[go to next](../exercise46/README.md)
