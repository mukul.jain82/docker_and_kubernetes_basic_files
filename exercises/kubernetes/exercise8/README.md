# Exercise 8: Delete resources

## Overview:

Resources can be deleted by file names, stdin, resources and names, or by resources and label selector. JSON and YAML formats are accepted.

### Examples:
  * Delete a pod using the type and name specified in pod.json.
  kubectl delete -f ./pod.json

  * Delete resources from a directory containing kustomization.yaml - e.g. dir/kustomization.yaml.
  kubectl delete -k dir

  * Delete a pod based on the type and name in the JSON passed into stdin.
  cat pod.json | kubectl delete -f -

  * Delete pods and services with same names "baz" and "foo"
  kubectl delete pod,service baz foo

  * Delete pods and services with label name=myLabel.
  kubectl delete pods,services -l name=myLabel

  * Delete a pod with minimal delay
  kubectl delete pod foo --now

  * Force delete a pod on a dead node
  kubectl delete pod foo --force

  * Delete all pods
  kubectl delete pods --all


## Delete busy Pod
```bash
kubectl delete -f busybox.yaml
```

[go to home](../../../README.md)

[go to next](../exercise9/README.md)
