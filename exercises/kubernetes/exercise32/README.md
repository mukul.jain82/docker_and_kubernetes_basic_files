# Exercise 32: Wordpress application from Helm

1. Change the directory
```bash
cd ~/wordpress/exercises/kubernetes/exercise32/
```

2. Create a namespace test-helm
```bash
kubectl create namespace test-helm
kubens test-helm
```

3. Add the repo to helm
```bash
helm repo add stable https://charts.helm.sh/stable
helm repo update    
```

4. Search a wordpress application
```bash
helm search repo wordpress
helm search hub wordpress
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update            
```

[bitnami/wordpress](https://artifacthub.io/packages/helm/bitnami/wordpress)

5. Install the wordpress
```bash
helm install my-release bitnami/wordpress
minikube service my-release-wordpress -n test-helm
```

![helm_wordpress](images/helm_wordpress.png)
![helm_wordpress_admin](images/helm_wordpress_admin.png)

6. Increse replica number to 5 for wordpress application

* Export to env variable WORDPRESS_PASSWORD
```bash
export WORDPRESS_PASSWORD=$(kubectl get secret --namespace test-helm my-release-wordpress \
 -o jsonpath="{.data.wordpress-password}" | base64 --decode)
```

* Export to env variable MARIADB_ROOT_PASSWORD
```bash
export MARIADB_ROOT_PASSWORD=$(kubectl get secret --namespace test-helm my-release-mariadb \
-o jsonpath="{.data.mariadb-root-password}" | base64 --decode)
```
* Export to env variableMARIADB_PASSWORD
```bash
export MARIADB_PASSWORD=$(kubectl get secret --namespace test-helm my-release-mariadb \
-o jsonpath="{.data.mariadb-password}" | base64 --decode)
```

* Run helm upgrade
```bash
$ helm upgrade my-release \
            --set replicaCount=5 \
            --set wordpressPassword=$WORDPRESS_PASSWORD \
            --set mariadb.auth.rootPassword=$MARIADB_ROOT_PASSWORD \
            --set mariadb.auth.password=$MARIADB_PASSWORD \
            bitnami/wordpress
```

7. Print all object for wordpress
```bash
kubectl get all -l app.kubernetes.io/instance=my-release
```

8. Clean
```bash
kubens -
kubectl delete namespace test-helm
```

[go to home](../../../README.md)

[go to next](../exercise33/README.md)
